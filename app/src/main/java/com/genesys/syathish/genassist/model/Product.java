package com.genesys.syathish.genassist.model;

import io.realm.RealmObject;

/**
 * Created by syathish on 12-01-2018.
 */

public class Product extends RealmObject {
    private String name;
    private String desc;
    private String uniqueId;
    private User productUser;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public User getProductUser() {
        return productUser;
    }

    public void setProductUser(User productUser) {
        this.productUser = productUser;
    }
}
