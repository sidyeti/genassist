package com.genesys.syathish.genassist.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by syathish on 12-01-2018.
 */


public class User extends RealmObject {


    private String name;
    private String phone;
    @PrimaryKey
    private String email;
    private String facebookAlias;
    private String pin;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookAlias() {
        return facebookAlias;
    }

    public void setFacebookAlias(String facebookAlias) {
        this.facebookAlias = facebookAlias;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

}
