package com.genesys.syathish.genassist.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.genesys.syathish.genassist.R;
import com.genesys.syathish.genassist.adapters.ProductRecyclerViewAdapter;
import com.genesys.syathish.genassist.model.Product;
import com.genesys.syathish.genassist.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class UserActivity extends AppCompatActivity {
    String PRODUCT_ADD_URL="/api/addProduct";
    String productName;
    String productDesc;
    String productId;
    String userEmailId;
    String API_BASE_URL;
    List<Product> productList=new ArrayList<>();
    ProductRecyclerViewAdapter productAdapter;
    Realm realm;
    FloatingActionButton floatingActionButton;
    EditText productDescEditText;
    @BindView(R.id.product_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.product_empty_text_view)
    TextView emptyTextView;
    @BindView(R.id.user_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        SharedPreferences prefs=getSharedPreferences("URL_PREF",MODE_PRIVATE);
        API_BASE_URL=prefs.getString("api",null);
        userEmailId=getIntent().getStringExtra("user_email");
        Realm.init(getApplicationContext());
        realm=Realm.getDefaultInstance();
        setSupportActionBar(toolbar);
        toolbar.setTitle(userEmailId);
        productAdapter=new ProductRecyclerViewAdapter(productList);
        initializeRecyclerView();
        getProducts();
        if(productList.isEmpty()){
            Log.d("AFTER DATA FETCH", "onCreate: data fetch is empty ");
            recyclerView.setVisibility(View.GONE);
            emptyTextView.setVisibility(View.VISIBLE);
        }
        else{
            recyclerView.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.GONE);
        }
        fabAction();

    }

    private void initializeRecyclerView() {
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(productAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && floatingActionButton.getVisibility() == View.VISIBLE) {
                    floatingActionButton.hide();
                } else if (dy < 0 && floatingActionButton.getVisibility() != View.VISIBLE) {
                    floatingActionButton.show();
                }

            }
        });
    }

    private void fabAction() {
        floatingActionButton=findViewById(R.id.product_add_floating_action_button);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent scannerIntent=new Intent(UserActivity.this,ScannerActivity.class);
                startActivityForResult(scannerIntent,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1){
            if (resultCode == Activity.RESULT_OK) {
                String qrString=data.getStringExtra("qrdata");
                Log.d("QR DATA IN USER", "onActivityResult: "+qrString);
                try {
                    JSONObject qrJson=new JSONObject(qrString);
                    productName=qrJson.getString("name");
                    productId=qrJson.getString("id");
                    Log.d("JSON_DATA", "productName: "+productName+" productID: "+productId);
                    getDescriptionDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    public void getDescriptionDialog(){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater=this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.product_desc_dialog_layout,null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("Product description");
        dialogBuilder.setMessage("What is this device?");
        dialogBuilder.setCancelable(true);
        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                productDescEditText=dialogView.findViewById(R.id.product_desc_edit_text);
                productDesc=productDescEditText.getText().toString().trim();
                if(productDesc.isEmpty()){
                    Toast.makeText(UserActivity.this, "Please enter a valid description", Toast.LENGTH_SHORT).show();
                }
                else{
                    addProductToDatabase();
                    addProductToOnlineDatabase();
                }
            }
        });
        dialogBuilder.create().show();

    }

    private void addProductToOnlineDatabase(){
        StringRequest stringRequest=new StringRequest(Request.Method.POST, API_BASE_URL+PRODUCT_ADD_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject responseObject=new JSONObject(response);
                    String responseMessage=responseObject.getString("message");
                    Log.d("VOLLEY_RESPONSE_PRODUCT_ADD", "onResponse: "+responseMessage);
                    if(responseMessage.contains("registered")){
                        Toast.makeText(UserActivity.this, "Product added online", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(UserActivity.this, "Unable to add product online", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(UserActivity.this, "Could not add products online", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", productName);
                params.put("description",productDesc);
                params.put("code",productId);
                params.put("userId",userEmailId);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private void addProductToDatabase() {
        realm.beginTransaction();
        User user=realm.where(User.class).equalTo("email",userEmailId).findFirst();
        Product product=realm.createObject(Product.class);
        product.setUniqueId(productId);
        product.setName(productName);
        product.setDesc(productDesc);
        product.setProductUser(user);
        realm.commitTransaction();
        productList.add(product);
        productAdapter.notifyDataSetChanged();
        if(emptyTextView.getVisibility()==View.VISIBLE){
            emptyTextView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        Toast.makeText(this, "Product added successfully", Toast.LENGTH_SHORT).show();
    }

    private void getProducts(){
        realm.beginTransaction();
        RealmResults<Product> productRealmList=realm.where(Product.class).equalTo("productUser.email",userEmailId).findAll();
        productList.addAll(productRealmList);
        realm.commitTransaction();
        productAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
