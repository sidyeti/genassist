package com.genesys.syathish.genassist.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.genesys.syathish.genassist.R;
import com.genesys.syathish.genassist.model.Product;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by syathish on 16-01-2018.
 */

public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.ProductViewHolder>{
    private Realm realm;
    private List<Product> userProducts;
    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_card_view,parent,false);
        Realm.init(parent.getContext());
        realm=Realm.getDefaultInstance();
        return new ProductViewHolder(itemView);
    }

    public ProductRecyclerViewAdapter(List<Product> products){
        this.userProducts=products;
    }
    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Product product=userProducts.get(position);
        holder.productNameTextView.setText(product.getName());
        holder.productDescTextView.setText(product.getDesc());
    }

    @Override
    public int getItemCount() {
        return userProducts.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_name_text_view)
        TextView productNameTextView;
        @BindView(R.id.product_description_text_view)
        TextView productDescTextView;
        @BindView(R.id.delete_product_image_view)
        ImageView deleteProductImageView;
        @OnClick(R.id.delete_product_image_view)
        void removeProductFromLocalDatabase(View itemView){
            int position=getAdapterPosition();
            String productId=userProducts.get(position).getUniqueId();
            RealmResults<Product> productsToBeDeleted = realm.where(Product.class).equalTo("uniqueId",productId).findAll();
            realm.beginTransaction();
            productsToBeDeleted.deleteAllFromRealm();
            realm.commitTransaction();
            Log.d("DELETE_PRODUCT", "removeProductFromLocalDatabase: Product removed from DB");
            userProducts.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position,userProducts.size());
        }
        ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
