package com.genesys.syathish.genassist.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.genesys.syathish.genassist.R;
import com.genesys.syathish.genassist.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class LoginActivity extends AppCompatActivity {

    String email;
    String pin;
    @BindView(R.id.login_email_edit_text)
    EditText loginEmail;
    @BindView(R.id.login_toolbar)
    Toolbar loginToolbar;
    @BindView(R.id.login_pin_edit_text)
    EditText loginPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       // loginToolbar=findViewById(R.id.login_toolbar);

        ButterKnife.bind(this);
        setSupportActionBar(loginToolbar);
        Realm.init(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.login_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.login_setttings_menu:
                Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(LoginActivity.this,SettingsActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.login_button)
    public void login(){
        Log.d("LOGIN", "login clicked");
        email=loginEmail.getText().toString().trim();
        pin=loginPin.getText().toString().trim();
        if(pin.isEmpty()||email.isEmpty()){
            Toast.makeText(this, "Enter all values", Toast.LENGTH_SHORT).show();
        }
        else{
            RealmResults<User> loginUser=Realm.getDefaultInstance().where(User.class).equalTo("email",email).findAll();
            if(loginUser.size()==0){
                Toast.makeText(this, "Login information incorrect!", Toast.LENGTH_SHORT).show();
            }
            else{
                Log.d("LOGIN_RESULT", loginUser.get(0).getName());
                Intent i = new Intent(LoginActivity.this,UserActivity.class);
                i.putExtra("user_email",email);
                startActivity(i);
                loginPin.setText("");
            }
        }

    }
    @OnClick(R.id.login_signup_button)
    public void openSignUpPage(){
        Intent i=new Intent(LoginActivity.this,SignupActivity.class);
        startActivity(i);
    }
}
