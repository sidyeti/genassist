package com.genesys.syathish.genassist.activities;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.genesys.syathish.genassist.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.api_url_edit_text)
    EditText urlEditText;
    @BindView(R.id.settings_toolbar)
    Toolbar settingsToolbar;
    String url;
    String API_BASE_URL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        setSupportActionBar(settingsToolbar);
        settingsToolbar.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.tick_mark_white);
        SharedPreferences prefs=getSharedPreferences("URL_PREF",MODE_PRIVATE);
        API_BASE_URL=prefs.getString("api",null);
        if(API_BASE_URL!=null) {
            Log.i("API_BASE_URL", API_BASE_URL);
            urlEditText.setText(API_BASE_URL);
        }
        else{
            Log.i("API_BASE_URL","null");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                setApiUrl();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick
    public void setApiUrl(){
        url=urlEditText.getText().toString();
        if(url.isEmpty()){
            Toast.makeText(this, "Please enter a valid url", Toast.LENGTH_SHORT).show();
        }
        else{
            writeUrltoSharedPrefs();
            finish();
        }
    }
    protected void writeUrltoSharedPrefs(){
        SharedPreferences.Editor editor=getSharedPreferences("URL_PREF",MODE_PRIVATE).edit();
        Log.d("API_BASE_URL", "writeUrltoSharedPrefs: "+url);
        editor.putString("api",url);
        editor.apply();
    }
}
