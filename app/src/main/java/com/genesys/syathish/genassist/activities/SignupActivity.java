package com.genesys.syathish.genassist.activities;

import android.app.DownloadManager;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.genesys.syathish.genassist.R;
import com.genesys.syathish.genassist.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmObject;

public class SignupActivity extends AppCompatActivity {
    String USER_SIGNUP_URL="/api/register";
    Realm realm;
    String email;
    String name;
    String phone;
    String facebook_alias;
    String pin;
    String API_BASE_URL;
    @BindView(R.id.signup_name_edit_text)
    EditText nameEditText;
    @BindView(R.id.signup_phone_edit_text)
    EditText phoneEditText;
    @BindView(R.id.signup_email_edit_text)
    EditText emailEditText;
    @BindView(R.id.signup_fb_edit_text)
    EditText facebookEditText;
    @BindView(R.id.signup_pin_edit_text)
    EditText pinEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        Realm.init(getApplicationContext());
        realm=Realm.getDefaultInstance();
        SharedPreferences prefs=getSharedPreferences("URL_PREF",MODE_PRIVATE);
        API_BASE_URL=prefs.getString("api",null);
    }

    @OnClick(R.id.signup_signup_button)
    public void signUp(){
        Log.d("SIGNUP_ACTIVITY","Signup_clicked");
        email=emailEditText.getText().toString().trim();
        name=nameEditText.getText().toString().trim();
        phone=phoneEditText.getText().toString().trim();
        facebook_alias=facebookEditText.getText().toString().trim();
        pin=pinEditText.getText().toString();

        if(email.isEmpty()||name.isEmpty()||phone.isEmpty()||facebook_alias.isEmpty()||pin.isEmpty()){
            Toast.makeText(this, "Please enter all values", Toast.LENGTH_SHORT).show();
        }
        else {
            if(API_BASE_URL!=null) {
                addUserToLocalDatabase();
                addUserToOnlineDatabase();
                finish();
            }
            else{
                Toast.makeText(this, "API Base URL not configured. Please configure it and try again", Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void addUserToLocalDatabase() {
        realm.beginTransaction();
        Log.d("REALM","create user BEGIN");
        User user = realm.createObject(User.class,email);
        user.setName(name);
        user.setPin(pin);
        user.setFacebookAlias(facebook_alias);
        user.setPhone(phone);
        realm.commitTransaction();
        Log.d("REALM","create user COMMIT");
        Toast.makeText(this, "User created!", Toast.LENGTH_SHORT).show();
    }

    private void addUserToOnlineDatabase(){
        StringRequest stringRequest=new StringRequest(Request.Method.POST, API_BASE_URL+USER_SIGNUP_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject responseObject=new JSONObject(response);
                    String responseMessage=responseObject.getString("message");
                    Log.d("VOLLEY_RESPONSE_USER_SIGNUP", "onResponse: "+responseMessage);
                    if(responseMessage.contains("successfully")){
                        Toast.makeText(SignupActivity.this, "User added online", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(SignupActivity.this, "Unable to add user online", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SignupActivity.this, "Could not add data online", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("emailID",email);
                params.put("phone",phone);
                params.put("fbAlias",facebook_alias);
                params.put("pin",pin);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
